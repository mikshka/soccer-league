# Soccer league test app

For building and running the application you need:

- Java 11
- Maven 3

## Running the application locally

```shell
mvn spring-boot:run
```