package com.wealthpilot.soccerleague.service;

import com.wealthpilot.soccerleague.model.League;
import com.wealthpilot.soccerleague.model.Match;
import com.wealthpilot.soccerleague.model.Team;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

@Service
public class TournamentService {

    private static final Logger logger = LoggerFactory.getLogger(TournamentService.class);

    @Value("${league.start.date}")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate startDate;

    @Value("${league.play.time}")
    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalTime playTime;

    @Value("${league.play.day}")
    private String playDay;

    @Value("${league.match.per.day}")
    private Integer matchPerDay;

    public List<Match> runTournamentPlan(League league) {

        if ((league == null) || CollectionUtils.isEmpty(league.getTeams())) {
            logger.error("Empty league or teams list!");
            throw new IllegalArgumentException("Empty league or teams list!");
        }

        DayOfWeek dayOfWeek = DayOfWeek.valueOf(playDay.toUpperCase());

        List<Match> matches = getFirstHalfOfTournament(league.getTeams(), dayOfWeek);
        LocalDateTime newMatchDate = matches.get(matches.size() - 1).getMatchDate().plusWeeks(3);

        List<Match> secondMatches = new ArrayList<>();
        int matchCounter = 0;
        for(Match match : matches) {
            if (matchCounter !=  0 && matchCounter % matchPerDay == 0) {
                newMatchDate = getNextDayOfWeek(newMatchDate, dayOfWeek);
                matchCounter = 0;
            }
            Match oppositeMatch = new Match(newMatchDate, match.getGuestTeam(), match.getHostTeam());
            secondMatches.add(oppositeMatch);
            matchCounter++;
        }

        matches.addAll(secondMatches);
        return matches;
    }

    private List<Match> getFirstHalfOfTournament(List<Team> teams, DayOfWeek dayOfWeek) {

        LocalDateTime startDateTime = startDate.atTime(playTime);
        if (!dayOfWeek.equals(startDateTime.getDayOfWeek())) {
            startDateTime = getNextDayOfWeek(startDateTime, dayOfWeek);
        }

        int size = teams.size();
        int[] grid = new int[size];
        int n = teams.size() / 2;
        for (int i = 0; i < n; i++) {
            grid[i] = i;
            grid[size - i - 1] = grid[i] + n;
        }

        List<Match> matches = new ArrayList<>();
        int matchCounter = 0;
        for (int j = 1; j <= size - 1; j++) {

            for (int i = 0; i < n; i++) {
                if (matchCounter !=  0 && matchCounter % matchPerDay == 0) {
                    startDateTime = getNextDayOfWeek(startDateTime, dayOfWeek);
                    matchCounter = 0;
                }
                Match match = new Match(startDateTime, teams.get(grid[i]), teams.get(grid[size - i - 1]));
                matches.add(match);
                matchCounter++;
            }
            int temp = grid[1];
            for (int i = 1; i < size - 1; i++) {
                int pr = grid[i + 1];
                grid[i + 1] = temp;
                temp = pr;
            }
            grid[1] = temp;
        }
        return matches;
    }

    private LocalDateTime getNextDayOfWeek(LocalDateTime date, DayOfWeek dayOfWeek) {
        return date.with((TemporalAdjusters.next(dayOfWeek)));
    }
}
