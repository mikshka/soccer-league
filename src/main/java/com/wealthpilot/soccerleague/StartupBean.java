package com.wealthpilot.soccerleague;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wealthpilot.soccerleague.model.League;
import com.wealthpilot.soccerleague.model.Match;
import com.wealthpilot.soccerleague.service.TournamentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Profile("!test")
@Component
public class StartupBean implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(StartupBean.class);
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private final TournamentService tournamentService;

    @Autowired
    public StartupBean(TournamentService planService) {
        this.tournamentService = planService;
    }

    @Override
    public void run(ApplicationArguments args)  {
        League league = loadLeagueFromFile();
        List<Match> matches = tournamentService.runTournamentPlan(league);
        System.out.println("Date            Team 1                Team 2");
        for(Match match : matches) {
            System.out.println(printMatch(match));
        }
    }

    public String printMatch(Match match) {
        StringBuilder sb = new StringBuilder();
        for(int i = match.getHostTeam().getName().length(); i < 22; i++) {
            sb.append(' ');
        }
        return match.getMatchDate().format(FORMATTER) + "      " + match.getHostTeam().getName() + sb.toString() + match.getGuestTeam().getName();
    }

    private League loadLeagueFromFile() {

        try (InputStream stream = new ClassPathResource("soccer_teams.json").getInputStream()) {
            ObjectMapper mapper = new ObjectMapper();
            TypeReference<League> typeReference = new TypeReference<>(){};
            League league = mapper.readValue(stream,typeReference);
            return league;
        } catch (IOException e){
            logger.error("Cannot read file!", e);
        }
        return null;
    }
}