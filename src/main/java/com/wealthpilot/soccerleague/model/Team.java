package com.wealthpilot.soccerleague.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Team {
    private String name;

    @JsonProperty("founding_date")
    private Integer foundingDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFoundingDate() {
        return foundingDate;
    }

    public void setFoundingDate(Integer foundingDate) {
        this.foundingDate = foundingDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Team team = (Team) o;
        return name.equals(team.name) && Objects.equals(foundingDate, team.foundingDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, foundingDate);
    }
}
