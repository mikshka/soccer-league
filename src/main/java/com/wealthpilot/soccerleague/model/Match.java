package com.wealthpilot.soccerleague.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class Match {

    private LocalDateTime matchDate;
    private Team hostTeam;
    private Team guestTeam;


    public Match(LocalDateTime matchDate, Team hostTeam, Team guestTeam) {
        this.matchDate = matchDate;
        this.hostTeam = hostTeam;
        this.guestTeam = guestTeam;
    }

    public Team getHostTeam() {
        return hostTeam;
    }

    public void setHostTeam(Team hostTeam) {
        this.hostTeam = hostTeam;
    }

    public Team getGuestTeam() {
        return guestTeam;
    }

    public void setGuestTeam(Team guestTeam) {
        this.guestTeam = guestTeam;
    }

    public LocalDateTime getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(LocalDateTime matchDate) {
        this.matchDate = matchDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return Objects.equals(matchDate, match.matchDate) && Objects.equals(hostTeam, match.hostTeam) && Objects.equals(guestTeam, match.guestTeam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(matchDate, hostTeam, guestTeam);
    }
}
