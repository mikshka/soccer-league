package com.wealthpilot.soccerleague.service;

import com.wealthpilot.soccerleague.SoccerLeagueApplication;
import com.wealthpilot.soccerleague.model.League;
import com.wealthpilot.soccerleague.model.Match;
import com.wealthpilot.soccerleague.model.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@ActiveProfiles("test")
@SpringBootTest(classes = SoccerLeagueApplication.class)
@TestPropertySource(
        locations = "classpath:application-test.properties")
public class TournamentServiceTest {

    @Autowired
    TournamentService tournamentService;

    @Test
    public void emptyLeagueTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> tournamentService.runTournamentPlan(new League()));
    }

    @Test
    public void nullLeagueTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> tournamentService.runTournamentPlan(null));    }

    @Test
    public void tournamentPlanningTest() {
        League league = new League();
        List<Team> teams = new ArrayList<>();

        Team team1 = generateTeam("team1");
        teams.add(team1);
        Team team2 = generateTeam("team2");
        teams.add(team2);
        league.setTeams(teams);

        List<Match> actual = tournamentService.runTournamentPlan(league);

        List<Match> expected = new ArrayList<>();
        LocalDateTime date1 = LocalDateTime.of(2022,
                Month.MARCH, 05, 17, 00, 00);
        Match match1 = new Match(date1, team1, team2);
        expected.add(match1);

        LocalDateTime date2 = LocalDateTime.of(2022,
                Month.MARCH, 26, 17, 00, 00);
        Match match2 = new Match(date2, team2, team1);
        expected.add(match2);
        Assertions.assertEquals(actual, expected);
    }

    private Team generateTeam(String name) {
        Team team = new Team();
        team.setName(name);
        return team;
    }
}
